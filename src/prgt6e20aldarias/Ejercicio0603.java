/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e20aldarias;

/**
 * Fichero: Ejercicio0603.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
// Clase sorteo
abstract class sorteo {

  protected int posibilidades;

  public abstract int lanzar();
}

// Clase dado
class dado extends sorteo {

  dado() {
    posibilidades = 6;
  }

  public int lanzar() {
    int dato = (int) (Math.random() * posibilidades + 1);
    System.out.println(dato);
    return dato;
  }
}

// Clase moneda
class moneda extends sorteo {

  moneda() {
    posibilidades = 2;
  }

  public int lanzar() {
    int dato = (int) (Math.random() * posibilidades + 1);
    if (dato == 1) {
      System.out.println("cara");
    } else {
      System.out.println("cruz");
    }
    return dato;
  }
}

// Main
public abstract class Ejercicio0603 {

  public static void main(String[] args) {
    dado dado1 = new dado();
    moneda moneda1 = new moneda();
    dado1.lanzar();
    moneda1.lanzar();
  }
}
/* EJECUCION:
 6
 cara
 * */
